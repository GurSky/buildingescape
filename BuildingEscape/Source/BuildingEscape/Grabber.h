// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"

USTRUCT()
struct FLineTraceStruct
{
	GENERATED_BODY()

	UPROPERTY()
	FVector Start;

	UPROPERTY()
	FVector End;

	FLineTraceStruct()
	{
		Start = FVector(0, 0, 0);
		End = FVector(0, 0, 0);
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	float Reach = 100.0; //how far ahead we can reach

	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;
	
	//ray-cast and grab what is in reach
	void Grab();
	void Release();

	//Find attached physics component
	void FindPhysicsHandleComponent();

	void MoveAttachedPhysicsBody();

	//Setup (assumed) attached input component
	void SetupInputComponent();

	//Generates and returns LineTrace to Reach from our viewpoint
	FLineTraceStruct GetLineTrace();

	//Return hit for first physics body in reach
	FHitResult GetFirstPhysicsBodyInReach();
};
